﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    public DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var connect = "Data Source=LAPTOP-NR588SC6\\SQLEXPRESS;Database=GlobalQueryFilter;Trusted_Connection=True;TrustServerCertificate=True";
        optionsBuilder.UseSqlServer(connect);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>()
                    .Property(x => x.UserName)
                    .HasMaxLength(100);

        modelBuilder.Entity<User>()
                    .Property(x => x.Password)
                    .HasMaxLength(100);

        modelBuilder.Entity<User>()
                    .HasData(
            new User { Id = 1, UserName = "A", Password = "a", IsActive = false },
            new User { Id = 2, UserName = "B", Password = "b", IsActive = true });

        modelBuilder.Entity<User>()
                    .HasQueryFilter(x => x.IsActive);
    }
}

public class User
{
    public int Id { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
    public bool IsActive { get; set; }
}