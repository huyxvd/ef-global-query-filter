﻿using Microsoft.EntityFrameworkCore;

AppDbContext context = new AppDbContext();

List<User> query = await context.Users.ToListAsync();
foreach (User user in query)
{
    Console.WriteLine($"{user.Id} {user.UserName}");
}

//var queryWithoutglobalFilter = await context.Users.IgnoreQueryFilters().ToListAsync();
//foreach (var user in queryWithoutglobalFilter)
//{
//    Console.WriteLine($"{user.Id} {user.UserName}");
//}